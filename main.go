package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"strings"
	"syscall"
	"time"

	etcdclient "github.com/coreos/etcd/client"
	"github.com/coreos/etcd/pkg/transport"
	"golang.org/x/net/context"
)

var etcdConnectTimeout = 30 * time.Second

var (
	etcdSRV     = flag.String("etcd-srv", "", "etcd DNS SRV lookup host")
	etcdURLs    = flag.String("etcd-urls", "http://localhost:2379", "etcd URLs (comma separated)")
	etcdSSLCert = flag.String("etcd-ssl-cert", "", "SSL client certificate")
	etcdSSLKey  = flag.String("etcd-ssl-key", "", "SSL client private key")
	etcdSSLCA   = flag.String("etcd-ssl-ca", "", "SSL CA")

	lockName = flag.String("name", "", "lock name")
	advAddr  = flag.String("service-addr", "", "service address to advertise")

	masterCmd = flag.String("master-cmd", "", "command to run when becoming master")
	slaveCmd  = flag.String("slave-cmd", "", "command to run when becoming slave, or on master changes")
)

func newEtcdClientFromFlags(ctx context.Context) (etcdclient.Client, error) {
	var tls transport.TLSInfo

	if *etcdSSLCert != "" && *etcdSSLKey != "" && *etcdSSLCA != "" {
		tls.CAFile = *etcdSSLCA
		tls.CertFile = *etcdSSLCert
		tls.KeyFile = *etcdSSLKey
		if *etcdSRV != "" {
			tls.ServerName = *etcdSRV
		}
	}

	t, err := transport.NewTransport(tls, etcdConnectTimeout)
	if err != nil {
		return nil, err
	}

	var endpoints []string
	if *etcdSRV != "" {
		var err error
		endpoints, err = etcdclient.NewSRVDiscover().Discover(*etcdSRV)
		if err != nil {
			return nil, err
		}
	} else {
		endpoints = strings.Split(*etcdURLs, ",")
	}

	c, err := etcdclient.New(etcdclient.Config{
		Transport:               t,
		Endpoints:               endpoints,
		HeaderTimeoutPerRequest: 3 * time.Second,
	})
	if err != nil {
		return nil, err
	}

	// Start an autosync goroutine in the background.
	go func() {
		autoSyncPeriod := 30 * time.Second
		for {
			err := c.AutoSync(ctx, autoSyncPeriod)
			if err == context.Canceled {
				break
			}
			log.Printf("autosync error: %v", err)
			time.Sleep(autoSyncPeriod)
		}
	}()

	return c, nil
}

func execStateChangeCmd(ctx context.Context, msg stateChangeMsg) error {
	var cmdStr string
	if msg.isMaster {
		cmdStr = *masterCmd
	} else {
		cmdStr = *slaveCmd
	}
	log.Printf("state change: %+v, running: %s", msg, cmdStr)
	cmd := exec.CommandContext(ctx, "/bin/sh", "-c", cmdStr)
	cmd.Env = os.Environ()
	if msg.isMaster {
		cmd.Env = append(cmd.Env, "IS_MASTER=1")
	} else {
		cmd.Env = append(cmd.Env, "IS_MASTER=0")
	}
	cmd.Env = append(cmd.Env, fmt.Sprintf("MASTER_ADDR=%s", msg.masterAddr))
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

// Set defaults for command-line flags using variables from the environment.
func setDefaultFlagsFromEnv() {
	flag.VisitAll(func(f *flag.Flag) {
		envVar := "MASTERELECTION_" + strings.ToUpper(strings.Replace(f.Name, "-", "_", -1))
		if value := os.Getenv(envVar); value != "" {
			f.DefValue = value
			if err := f.Value.Set(value); err != nil {
				log.Printf("Warning: invalid value for %s: %v", envVar, err)
			}
		}
	})
}

func main() {
	log.SetFlags(0)
	setDefaultFlagsFromEnv()
	flag.Parse()

	if *lockName == "" {
		log.Fatal("Must specify --name")
	}
	if *advAddr == "" {
		log.Fatal("Must specify --service-addr")
	}

	// Create a global permanent context that we can cancel on SIGTERM.
	ctx, cancel := context.WithCancel(context.Background())
	termCh := make(chan os.Signal, 1)
	go func() {
		<-termCh
		cancel()
	}()
	signal.Notify(termCh, syscall.SIGTERM, syscall.SIGINT)

	// Connect to etcd.
	etcd, err := newEtcdClientFromFlags(ctx)
	if err != nil {
		log.Fatal("could not connect to etcd:", err)
	}

	lockPath := fmt.Sprintf("/me/%s/lock", *lockName)

	runMasterElection(ctx, etcdclient.NewKeysAPI(etcd), lockPath, *advAddr, execStateChangeCmd)
}
