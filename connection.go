package main

import (
	"log"
	"math/rand"
	"time"

	etcdclient "github.com/coreos/etcd/client"
	"golang.org/x/net/context"
)

var (
	watchErrorBackoffTime   = 3 * time.Second
	connectErrorBackoffTime = 3 * time.Second
	masterUpdateTTL         = 10 * time.Second
	masterUpdateInterval    = 3 * time.Second
)

type stateChangeMsg struct {
	isMaster   bool
	masterAddr string
}

func (m stateChangeMsg) Nil() bool {
	return !m.isMaster && m.masterAddr == ""
}

type stateChangeFn func(context.Context, stateChangeMsg) error

func runMaster(ctx context.Context, api etcdclient.KeysAPI, lockPath, self string, stateFn stateChangeFn) error {
	// Create a shared cancelable context. We want to start
	// refreshing the lock TTL as soon as possible and without
	// interruption, even if the state change notification takes
	// longer than the update interval. At the same time, we need
	// to exit the refresh loop if the state change operation
	// fails.
	masterCtx, cancel := context.WithCancel(ctx)
	defer cancel()

	// Spawn two goroutines, for TTL renewal and for the state
	// change operation. Retrieve the first error, but wait for
	// both of them to complete before returning it.
	errCh := make(chan error)
	defer close(errCh)
	go func() {
		errCh <- stateFn(masterCtx, stateChangeMsg{isMaster: true, masterAddr: self})
	}()
	go func() {
		errCh <- runMasterState(masterCtx, api, lockPath, self)
	}()

	var err error
	for i := 0; i < 2; i++ {
		if ierr := <-errCh; ierr != nil && err == nil {
			err = ierr
			// Cancel the other goroutine.
			cancel()
		}
	}
	return err
}

func runMasterState(ctx context.Context, api etcdclient.KeysAPI, lockPath, self string) error {
	ticker := time.NewTicker(masterUpdateInterval)
	defer ticker.Stop()
	for range ticker.C {
		innerCtx, cancel := context.WithTimeout(ctx, masterUpdateInterval)
		_, err := api.Set(innerCtx, lockPath, "", &etcdclient.SetOptions{
			TTL:              masterUpdateTTL,
			PrevValue:        self,
			PrevExist:        etcdclient.PrevExist,
			Refresh:          true,
			NoValueOnSuccess: true,
		})
		cancel()
		if err != nil {
			return err
		}
	}
	// Unreachable.
	return nil
}

func runSlaveState(ctx context.Context, api etcdclient.KeysAPI, lockPath, curMaster string, index uint64, stateCh chan stateChangeMsg) error {
	stateCh <- stateChangeMsg{masterAddr: curMaster}

	for {
		watch := api.Watcher(lockPath, &etcdclient.WatcherOptions{
			AfterIndex: index,
		})
		for {
			resp, err := watch.Next(ctx)
			if err == context.Canceled {
				return err
			}

			// Let's consider all errors retriable for
			// now. Assume the Watcher is not in a valid
			// state after Next returns an error, so
			// rebuild it.
			if err != nil {
				break
			}

			// If the lock expires or is deleted, try to
			// acquire it again.
			if resp.Action == "delete" || resp.Action == "compareAndDelete" || resp.Action == "expire" {
				return nil
			}

			// See if the current master has changed.
			if curMaster != resp.Node.Value {
				log.Printf("master has changed: %s", resp.Node.Value)
				curMaster = resp.Node.Value
				stateCh <- stateChangeMsg{masterAddr: curMaster}
			}
			index = resp.Index
		}
		sleepRandom(watchErrorBackoffTime)
	}
}

func runSlave(ctx context.Context, api etcdclient.KeysAPI, lockPath string, stateFn stateChangeFn) error {
	// Get the object to initialize current master
	rpcCtx, cancel := context.WithTimeout(ctx, etcdclient.DefaultRequestTimeout)
	resp, err := api.Get(rpcCtx, lockPath, nil)
	cancel()
	if err != nil {
		return nil
	}
	curMaster := resp.Node.Value
	index := resp.Node.ModifiedIndex

	// Status updates can be interrupted by another status
	// change. The alternative would be more complex, as it would
	// mean allowing slow-updating slaves to fall behind.
	stateUpdateCh := make(chan stateChangeMsg)
	doneCh := make(chan bool)
	go func() {
		defer close(doneCh)
		var uctx context.Context
		var ucancel context.CancelFunc
		var updateDoneCh chan bool

		for {
			select {
			case msg := <-stateUpdateCh:
				if msg.Nil() {
					goto cleanup
				}
				uctx, ucancel = context.WithCancel(ctx)
				updateDoneCh = make(chan bool)
				go func(updateDoneCh chan bool) {
					if err := stateFn(uctx, msg); err != nil {
						log.Printf("error updating state %+v: %v", msg, err)
					}
					close(updateDoneCh)
				}(updateDoneCh)
			case <-updateDoneCh:
				ucancel()
				uctx = nil
				updateDoneCh = nil
			case <-ctx.Done():
				goto cleanup
			}
		}

	cleanup:
		if uctx != nil {
			ucancel()
			<-updateDoneCh
		}
	}()

	err = runSlaveState(ctx, api, lockPath, curMaster, index, stateUpdateCh)
	close(stateUpdateCh)
	<-doneCh
	return err
}

func createLock(ctx context.Context, api etcdclient.KeysAPI, lockPath, self string) (*etcdclient.Response, error) {
	for {
		innerCtx, cancel := context.WithTimeout(ctx, masterUpdateTTL)
		resp, err := api.Set(innerCtx, lockPath, self, &etcdclient.SetOptions{
			TTL:       masterUpdateTTL,
			PrevExist: etcdclient.PrevNoExist,
		})
		cancel()
		if err == nil {
			return resp, nil
		}
		if err == context.Canceled {
			return nil, err
		}
		if _, ok := err.(etcdclient.Error); ok {
			return resp, err
		}

		// Anything that is not an etcdclient.Error is
		// considered a transport issue, so we retry.
		log.Printf("etcd error: %v", err)
		sleepRandom(connectErrorBackoffTime)
	}
}

func runMasterElection(ctx context.Context, api etcdclient.KeysAPI, lockPath, self string, stateFn stateChangeFn) {
	// Delete the lock (if we're holding it) on exit. This isn't
	// strictly necessary but facilitates faster failover if we
	// are the master.
	defer func() {
		// We can ignore errors here.
		dctx, cancel := context.WithTimeout(context.Background(), etcdclient.DefaultRequestTimeout)
		defer cancel()
		api.Delete(dctx, lockPath, &etcdclient.DeleteOptions{
			PrevValue: self,
		})
	}()

	for {
		// Try creating the lock.
		_, err := createLock(ctx, api, lockPath, self)
		if err == context.Canceled {
			return
		} else if err != nil {
			// Could not create lock, we are not the master.
			err = runSlave(ctx, api, lockPath, stateFn)
		} else {
			// Success, we are now the master.
			err = runMaster(ctx, api, lockPath, self, stateFn)
			// Once we are not the master anymore, there's
			// the possibility that we have lost access to
			// etcd. Issue a state change to slave with
			// unknown master, for safety.
			//
			// TODO: it would be better to wait for a
			// little while, just in case we can
			// successfully reconnect right away and do a
			// single master -> slave transition.
			stateFn(ctx, stateChangeMsg{isMaster: false})
		}

		if err == context.Canceled {
			return
		}

		if err != nil {
			log.Printf("state runner error: %v", err)
		}

		// Make life a bit easier for etcd by waiting a little
		// before trying again to acquire the lock. This isn't
		// strictly necessary from a correctness point of
		// view.
		sleepRandom(1 * time.Second)
	}
}

func sleepRandom(d time.Duration) {
	time.Sleep(time.Duration(rand.Int63n(int64(d))))
}
